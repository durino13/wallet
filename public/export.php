<?php

use yuma\model\ExportManager;
use yuma\model\Logger;
use yuma\model\ParseManager;
use yuma\model\WalletManager;

require_once './bootstrap.php';
require_once '../vendor/autoload.php';

error_reporting(E_ALL); ini_set('display_errors',1);
header('Content-Type: text/html; charset=utf-8');
echo '
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body style="font-family: \'Open Sans\'">';

// Print application version;
Logger::log('Application version: '. getenv('VERSION'));

// Export mbank data

try {
    ExportManager::export();
} catch (Exception $e) {
    Logger::log($e->getMessage());
}

// Parse data

try {
    ParseManager::parse();
} catch (Exception $e) {
    Logger::log($e->getMessage());
}

// Import data

try {
    WalletManager::emailImport();
} catch (Exception $e) {
    Logger::log($e->getMessage());
}

echo '</body></html>';

