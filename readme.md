# Start the development server with XDEBUG enabled

    - cd _docker/bin
    - ./start-dev
    
# Build the docker web container

    - ./build

# Push image into private docker registry

    docker push docker-registry.yuma.sk/wallet/web:v0.0.1

# Deploy the application to the prod server

    # copy the file 'docker-compose-deploy.yml' to the remote server
    # run 'docker-compose up -d'