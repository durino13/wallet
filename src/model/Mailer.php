<?php

namespace yuma\model;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer
{

    /** @var PHPMailer $mailer */
    protected $mailer;
    protected $smtp_server;
    protected $smtp_port;
    protected $mail_account;
    protected $mail_password;
    protected $mail_name;

    /**
     * Mailer constructor.
     * @param $smtp_server
     * @param $smtp_port
     * @param $mail_account
     * @param $mail_password
     * @param $mail_name
     */
    public function __construct($smtp_server, $smtp_port, $mail_account, $mail_password, $mail_name)
    {
        $this->smtp_server = $smtp_server;
        $this->smtp_port = $smtp_port;
        $this->mail_account = $mail_account;
        $this->mail_password = $mail_password;
        $this->mail_name = $mail_name;

        // Use the mailer
        $this->mailer = new PHPMailer(true);                              // Passing `true` enables exceptions

        //Server settings
        $this->mailer->Host = $this->smtp_server;                       // Specify main and backup SMTP servers
        $this->mailer->Username = $this->mail_account;                 // SMTP username
        $this->mailer->Password = $this->mail_password;                // SMTP password
        $this->mailer->Port = $this->smtp_port;                        // TCP port to connect to
        $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mailer->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
//        $this->mailer->SMTPDebug = 2;                                 // Enable verbose debug output
        $this->mailer->isSMTP();                                      // Set mailer to use SMTP
    }

    /**
     * Send mail
     * @param string $to
     * @param string $csvExport
     * @param string $fileName
     * @throws Exception
     */
    public function send(string $to, string $csvExport, string $fileName)
    {
        //Recipients
        $this->mailer->setFrom($this->mail_account);
        $this->mailer->addAddress($to);     // Add a recipient

        //Attachments
        $this->mailer->addStringAttachment($csvExport, $fileName, 'base64', 'text/plain');

        //Content
        // TODO Message body empty
        $this->mailer->isHTML();                                  // Set email format to HTML
        $this->mailer->Subject = 'mBank export';
        $this->mailer->msgHTML('mBank export');
        $this->mailer->Body = 'mBank export';
        $this->mailer->AltBody = 'mBank export';

        $this->mailer->send();
    }

}