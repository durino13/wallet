<?php

namespace yuma\model;

class RecipientCategoryManager implements ICategoryManager
{

    protected $recipientToCategoryMap;

    /**
     * RecipientCategoryManager constructor.
     * @param $recipientToCategoryMap
     */
    public function __construct(array $recipientToCategoryMap)
    {
        // TODO Hardcoded, remove into config
        $this->recipientToCategoryMap = [
            'SK5381800000007000531807' => Category::CategoryHousing
        ];
    }

    /**
     * @param $iban
     * @return mixed|string
     */
    protected function findMatchingCategory($iban)
    {
        if (array_key_exists($iban, $this->recipientToCategoryMap)) {
            return $this->recipientToCategoryMap[$iban];
        }

        return Category::CategoryUnknown;
    }

    /**
     * @param WalletCsv $walletCsv
     * @return mixed|void
     */
    public function categorize(WalletCsv $walletCsv)
    {
        $rows = $walletCsv->getRows();

        /** @var \WalletCsvRow $row */
        foreach ($rows as $row) {
            $recipientIban = $row->getRecipientIban();
            $newCategory = $this->findMatchingCategory($recipientIban);
            $row->setCategory($newCategory);
        }
    }

}