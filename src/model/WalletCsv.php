<?php

namespace yuma\model;

class WalletCsv
{

    protected $rows;

    /**
     * WalletCsv constructor.
     * @param array $rows
     * @internal param $headers
     */
    public function __construct(array $rows = null)
    {
        $this->rows = [];
    }

    /**
     * @param WalletCsvRow $row
     */
    public function addRow(WalletCsvRow $row)
    {
        $this->rows[] = $row;
    }

    /**
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     *
     */
    public function getRowCount(): int
    {
        return count($this->rows);
    }

    /**
     * Get the contents of the CSV file
     */
    public function getContent(): string
    {

        $content = '';

        /** @var WalletCsvRow $row */
        foreach ($this->rows as $row) {
            $content .= $row->getRow(';') . PHP_EOL;
        }

        return $content;
    }


}