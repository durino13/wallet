<?php

namespace yuma\model;

class WalletCsvRow
{
    protected $recipient;
    protected $date;
    protected $amount;
    protected $currency;
    protected $paymentType;
    protected $note;
    protected $account;
    protected $category;

    /**
     * WalletCsvRow constructor.
     * @param $recipient
     * @param $date
     * @param $amount
     * @param $currency
     * @param $payment_type
     * @param $note
     * @param $account
     * @param $category
     * @internal param $sender
     */
    public function __construct($recipient, $date, $amount, $currency, $payment_type, $note, $account, $category)
    {
        $this->recipient = $recipient;
        $this->date = $date;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->paymentType = $payment_type;
        $this->note = $note;
        $this->account = $account;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getRecipientIban()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param mixed $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param mixed $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @param $delimiter
     * @return string
     */
    public function getRow($delimiter): string
    {
        return $this->getDate() . $delimiter.
        $this->getAmount() . $delimiter.
        $this->getCurrency() . $delimiter.
        $this->getPaymentType() . $delimiter.
        $this->getNote() .$delimiter.
        $this->getRecipientIban() . $delimiter.
        $this->getAccount() . $delimiter.
        $this->getCategory(). $delimiter;
    }

}