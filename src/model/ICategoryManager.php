<?php

namespace yuma\model;

interface ICategoryManager
{

    /**
     * @param WalletCsv $walletCsv
     * @return mixed
     */
    public function categorize(WalletCsv $walletCsv);

}