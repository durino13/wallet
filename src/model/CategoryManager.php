<?php

namespace yuma\model;

class CategoryManager
{

    protected $walletCsv;
    protected $managers;

    /**
     * @return WalletCsv
     */
    public function getWalletCsv(): WalletCsv
    {
        return $this->walletCsv;
    }

    /**
     * @param WalletCsv $walletCsv
     */
    public function setWalletCsv(WalletCsv $walletCsv)
    {
        $this->walletCsv = $walletCsv;
    }

    /**
     * @param ICategoryManager $manager
     * @internal param IRule $rule
     */
    public function addManager(ICategoryManager $manager)
    {
        $this->managers[] = $manager;
    }

    /**
     * Categorize the payments
     */
    public function categorizeTransactions()
    {
        foreach ($this->managers as $manager) {
            $manager->categorize($this->walletCsv);
        }
        return $this->walletCsv;
    }

}