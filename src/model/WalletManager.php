<?php
/**
 * Created by PhpStorm.
 * User: jmarusiak
 * Date: 4. 12. 2017
 * Time: 19:55
 */

namespace yuma\model;


class WalletManager
{

    public static function emailImport()
    {
        $dao = new ExportDao(
            getenv('DB_HOST'),
            getenv('DB_PORT'),
            getenv('DB_NAME'),
            getenv('DB_USER'),
            getenv('DB_PASSWORD'));

        /** @var BankExportEntry[] $bankExports */
        $bankExports = $dao->getExportsToBeMailed();

        foreach ($bankExports as $bankExport) {

            // Import by email

            $mailer = new Mailer(getenv('SMTP_SERVER'),
                getenv('SMTP_PORT'),
                getenv('MAIL_ACCOUNT'),
                getenv('MAIL_PASSWORD'),
                getenv('MAIL_NAME'));

            $mailer->send(
                getenv('WALLET_EMAIL_ADDRESS'),
                $bankExport->getParsedData(),
                'mbank_export.csv');

            $dao->markEmailCompleted($bankExport->getExportId());

            Logger::log('Email sent.', Logger::SEVERITY_NORMAL);

        }
    }

}