<?php
/**
 * Created by PhpStorm.
 * User: jmarusiak
 * Date: 1. 12. 2017
 * Time: 15:47
 */

namespace yuma\model;


class Logger
{

    const SEVERITY_ERROR = 'SEVERITY_ERROR';
    const SEVERITY_NORMAL = 'SEVERITY_NORMAL';
    const SEVERITY_DEBUG = 'SEVERITY_DEBUG';

    public static function log(string $msg, string $severity = self::SEVERITY_NORMAL)
    {
        echo $msg . '</br>';
        flush();
    }

}