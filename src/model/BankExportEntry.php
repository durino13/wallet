<?php

namespace yuma\model;

class BankExportEntry
{

    protected $export_id;
    protected $exported_date;
    protected $exported_data;
    protected $parsed_date;
    protected $parsed_data;
    protected $export_type;
    protected $is_parsed;
    protected $is_processed;

    /**
     * @return mixed
     */
    public function getExportId()
    {
        return $this->export_id;
    }

    /**
     * @param mixed $export_id
     */
    public function setExportId($export_id)
    {
        $this->export_id = $export_id;
    }

    /**
     * @return mixed
     */
    public function getExportedData()
    {
        return $this->exported_data;
    }

    /**
     * @param mixed $exported_data
     */
    public function setExportedData($exported_data)
    {
        $this->exported_data = $exported_data;
    }

    /**
     * @return mixed
     */
    public function isParsed()
    {
        return $this->is_parsed;
    }

    /**
     * @return mixed
     */
    public function isEmailed()
    {
        return $this->is_processed;
    }

    /**
     * @return mixed
     */
    public function getExportedDate()
    {
        return $this->exported_date;
    }

    /**
     * @param mixed $exported_date
     */
    public function setExportedDate($exported_date)
    {
        $this->exported_date = $exported_date;
    }

    /**
     * @return mixed
     */
    public function getParsedDate()
    {
        return $this->parsed_date;
    }

    /**
     * @param mixed $parsed_date
     */
    public function setParsedDate($parsed_date)
    {
        $this->parsed_date = $parsed_date;
    }

    /**
     * @return mixed
     */
    public function getParsedData()
    {
        return $this->parsed_data;
    }

    /**
     * @param mixed $parsed_data
     */
    public function setParsedData($parsed_data)
    {
        $this->parsed_data = $parsed_data;
    }

    /**
     * @return mixed
     */
    public function getExportType()
    {
        return $this->export_type;
    }

    /**
     * @param mixed $export_type
     */
    public function setExportType($export_type)
    {
        $this->export_type = $export_type;
    }

    /**
     * Get last successful export entry from the database
     * @param ExportDao $dao
     * @return bool|DateTime
     */
    public static function getLastExportDate(ExportDao $dao)
    {
        return $dao->getLastExportEntry();
    }

}