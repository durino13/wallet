<?php

namespace yuma\model;

use Exception;

class MBankParser
{

    const CSV_FIELD_COUNT = 12;

    protected $data;
    protected $csvFieldCount;
    protected $outputCsv;

    /**
     * MBankParser constructor.
     * @param $data
     * @internal param WalletCsv $outputCsv
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return WalletCsv
     * @throws Exception
     * @internal param WalletCsv $csv
     */
    public function parse(): WalletCsv
    {
        $parsedData = $this->data;
        $parsedData = $this->extractImportantData($parsedData);
        // TODO Treba?
        $parsedData = $this->removeSpecialCharacters($parsedData);
        $rows = $this->extractRows($parsedData);

        if (empty($rows[0][0])) {
            throw new Exception('No transactions available. ');
        }

        $walletCsv = new WalletCsv();

        foreach ($rows as $row) {
            $walletCsv->addRow(
                new WalletCsvRow(
                    preg_replace('!\'+!','',$row[5]),         // Recipient
                    $row[0],                                                   // Transaction date
                    $row[9],                                                   // Amount
                    'Eur',                                             // Currency
                    $row[2],                                                   // ?
                    preg_replace('!\s+!', ' ', $row[3]),      // Description
                    'mBank',                                            // Account
                    'Category')                                         // Category
            );
        }

        return $walletCsv;
    }

    /**
     * Get rid of encoded data ..
     * @param $data
     * @return mixed
     */
    protected function removeSpecialCharacters($data)
    {
        return str_replace('&quot;','',$data);
    }

    /**
     * Extract only CSV portion of the file ..
     * @param $data
     * @return mixed
     */
    protected function extractImportantData($data)
    {
        $regex = '/zostatok po transakcii;.(.*).;;;;;;/s';
        preg_match_all($regex, $data, $matches);
        return $matches[1][0];
    }

    /**
     * @param $data
     * @return array
     */
    protected function extractRows($data): array
    {
        $regex = '/"(?:[^"]|"")*"|[^;\n]+|(?=;)(?<=;)|^|(?<=;)$/';
        preg_match_all($regex, trim($data), $matches);
        return array_chunk($matches[0], self::CSV_FIELD_COUNT);
    }

}