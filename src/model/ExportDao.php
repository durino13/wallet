<?php

namespace yuma\model;

use DateTime;
use PDO;

class ExportDao
{

    public $host;
    public $port;
    public $user;
    public $password;
    public $database;
    public $dsn;
    public $db;

    /**
     * ExportDao constructor.
     * @param $host
     * @param $port
     * @param $user
     * @param $password
     * @param $database
     */
    public function __construct($host, $port, $database, $user, $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
        $this->dsn = 'mysql:host='.$host.';dbname='.$database;
        $this->db = new PDO($this->dsn, $user, $password);
        $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    }

    /**
     * @param BankExportEntry $export
     */
    public function insertExportEntry(BankExportEntry $export)
    {
        $exported_date = $export->getExportedDate();
        $exported_data = $export->getExportedData();

        $sql = 'INSERT INTO exports (exported_date, exported_data) VALUES (:exported_date, :exported_data)';
        $stmt = $this->db->prepare($sql);

        $stmt->bindParam(':exported_date', $exported_date);
        $stmt->bindParam(':exported_data', $exported_data);

        $res = $stmt->execute();
    }

    /**
     * Update the record with the parsed data
     * @param BankExportEntry $export
     * @param string $parsedData
     */
    public function updateExportEntry(BankExportEntry $export, $parsedData)
    {
        $now = new DateTime();
        $sql = 'UPDATE exports SET 
                  parsed_date = :now,
                  parsed_data = :parsed_data
                WHERE export_id = :export_id';

        $stmt = $this->db->prepare($sql);
        $todayDate = $now->format('Y-m-d');
        $exportId = $export->getExportId();
        $stmt->bindParam(':now', $todayDate);
        $stmt->bindParam(':parsed_data', $parsedData);
        $stmt->bindParam(':export_id', $exportId);
        $res = $stmt->execute();
    }

    /**
     * Get all non processed entries ..
     */
    public function getPendingExports()
    {
        $sql = 'SELECT * FROM exports WHERE is_parsed = 0';
        $staetment = $this->db->query($sql);
        return $staetment->fetchAll(PDO::FETCH_CLASS, 'yuma\model\BankExportEntry');
    }

    /**
     * Get exports to be mailed ..
     * @return array
     */
    public function getExportsToBeMailed()
    {
        $sql = 'SELECT * FROM exports WHERE is_emailed = 0';
        $statement = $this->db->query($sql);
        return $statement->fetchAll(PDO::FETCH_CLASS, 'yuma\model\BankExportEntry');
    }

    /**
     * Get the date of the last successfull export entry
     */
    public function getLastExportEntry()
    {
        $sql = 'SELECT exported_date FROM exports WHERE exported_date IS NOT NULL ORDER BY export_id DESC LIMIT 1';
        $res = $this->db->query($sql, PDO::FETCH_ASSOC);

        if ($res->rowCount() > 0) {
            $dateStr = $res->fetchAll()[0]['exported_date'];
            return DateTime::createFromFormat('Y-m-d H:i:s', $dateStr);
        }

        return new DateTime('2015-01-01 00:00:00');
    }

    /**
     * Mark export processed after successful parsing operation
     * @param int $export_id
     * @internal param BankExportEntry $export
     */
    public function markParsingCompleted(int $export_id)
    {
        $sql = 'UPDATE exports SET is_parsed = 1 WHERE export_id = :export_id';
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':export_id', $export_id);
        $statement->execute();
    }

    /**
     * Mark mail sent
     * @param int $export_id
     */
    public function markEmailCompleted(int $export_id)
    {
        $sql = 'UPDATE exports SET is_emailed = 1 WHERE export_id = :export_id';
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':export_id', $export_id);
        $statement->execute();
    }

    /**
     * Log error into database
     * @param int $export_id
     * @param string $error_log
     */
    public function logError(int $export_id, string $error_log)
    {
        $sql = 'UPDATE exports SET error_log = :error_log WHERE export_id = :export_id';
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':export_id', $export_id);
        $statement->bindParam(':error_log', $error_log);
        $statement->execute();
    }


}