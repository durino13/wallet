<?php

namespace yuma\model;

use Exception;

class ParseManager
{

    public static function parse()
    {
        $dao = new ExportDao(
            getenv('DB_HOST'),
            getenv('DB_PORT'),
            getenv('DB_NAME'),
            getenv('DB_USER'),
            getenv('DB_PASSWORD'));

        $pendingExports = $dao->getPendingExports();

        Logger::log('Pending exports found: '. count($pendingExports));

        /** @var BankExportEntry $pendingExport */
        foreach ($pendingExports as $pendingExport) {

            $export_id = $pendingExport->getExportId();
            $mbankParser = new MBankParser($pendingExport->getExportedData());
            try {

                // Parse transactions

                /** @var WalletCsv $walletCsv */
                $walletCsv = $mbankParser->parse();

                // Categorize transactions here ..

                $categoryManager = new CategoryManager();
                $categoryManager->setWalletCsv($walletCsv);
                $recipientCategoryManager = new RecipientCategoryManager([]);
                $categoryManager->addManager($recipientCategoryManager);
                $walletCsv = $categoryManager->categorizeTransactions();

                // Update transactions

                $dao->updateExportEntry($pendingExport, $walletCsv->getContent());

            } catch (Exception $e) {
                $dao->logError($export_id, $e->getMessage());
                echo($e->getMessage());
            }

            // Mark entry parsing completed ..
            $dao->markParsingCompleted($export_id);

        }

    }

}