<?php

namespace yuma\model;

use bgaluszka\Mbank\Mbank;
use DateTime;

class ExportManager
{

    /**
     * @throws \Exception
     */
    public static function export()
    {
        $mbank_iban = getenv('MBANK_IBAN');
        $mbank_id = getenv('MBANK_ID');
        $mbank_password = getenv('MBANK_PASSWORD');

        // Grab the database handle
        $dao = new ExportDao(getenv('DB_HOST'), getenv('DB_PORT'), getenv('DB_NAME'), getenv('DB_USER'), getenv('DB_PASSWORD'));

        // Login to mBank
        $mbank = new Mbank();
        $mbank->login($mbank_id, $mbank_password);

        // Get last successful export entry
        $lastExportDate = BankExportEntry::getLastExportDate($dao);

        Logger::log('Last export date found: '. $lastExportDate->format('d.m.Y'));

        // Do not allow multiple exports during the same day ..
        $today = new DateTime('now');
        $interval = $today->diff($lastExportDate);
        if ($interval->days === 0) {
            throw new \Exception('Multiple exports in the same day are not allowed!');
        }

        // all transaction since beginning of given month
        $csv = $mbank->export($mbank_iban,

            // export criterias
            array(
                'daterange_from_day'   => $lastExportDate->format('d'),
                'daterange_from_month' => $lastExportDate->format('m'),
                'daterange_from_year'  => $lastExportDate->format('Y'),
                'daterange_to_day'     => date('d'),
                'daterange_to_month'   => date('m'),
                'daterange_to_year'    => date('Y'),

                // we want to export incoming transfers only
                'accoperlist_typefilter_group' => Mbank::TRANS_ALL,
            ));

        // Logout from the mBank
        $mbank->logout();

        // you probably want to convert that to UTF-8
        $csv = iconv('WINDOWS-1250', 'UTF-8', $csv);
        $exportEntry = new BankExportEntry();
        $exportEntry->setExportedDate(date('Y-m-d H:i:s'));
        $exportEntry->setExportedData($csv);

        // Insert export into db
        $dao->insertExportEntry($exportEntry);

        Logger::log('Data successfully exported.');
    }

}