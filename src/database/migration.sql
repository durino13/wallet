-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `wallet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `wallet`;

CREATE TABLE `exports` (
  `export_id` int(11) NOT NULL AUTO_INCREMENT,
  `exported_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exported_data` longtext DEFAULT NULL,
  `parsed_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parsed_data` longtext DEFAULT NULL,
  `export_type` text DEFAULT NULL,
  `is_parsed` tinyint(4) DEFAULT 0,
  `is_processed` tinyint(4) DEFAULT 0,
  `is_emailed` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`export_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2018-01-10 07:19:57