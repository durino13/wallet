ARG ENV="prod"

FROM docker-registry.yuma.sk/servers/apache-${ENV}:v0.1

ARG HTTP_PROXY=""
ARG HTTPS_PROXY=""
ARG NO_PROXY=""

ENV HTTP_PROXY=${HTTP_PROXY} \
    HTTPS_PROXY=${HTTPS_PROXY} \
    NO_PROXY=${HTTPS_PROXY}

## install required php modules
RUN image--php-add-mod cli pdo_mysql curl xml dom json iconv

## enable required apache modules
RUN image--apache-enable-mod deflate filter rewrite

## set document root
RUN image--apache-set-docroot /app/public

RUN image--on-init \
   'chmod 777 /app/public'

RUN apk add --no-cache bash

## copy application into image, create necessary directories
WORKDIR /app
COPY src src
COPY public public
COPY vendor vendor

# TODO Chcem vybuildovat image s XDEBUG pre DEV a bez XDEBUG pre PROD .. Pouzivam ENV / ARG .. Treba pretestovat ..