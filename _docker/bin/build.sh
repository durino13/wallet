#!/usr/bin/env sh
export ENV=prod
registry=docker-registry.yuma.sk
project_name=wallet

version=`cat ../../src/resources/version.txt`

echo "Creating new docker image version ($version) ..."

docker build -t ${registry}/${project_name}/web:${version} -t ${registry}/${project_name}/web:latest ../..
docker login ${registry} -u mydocker -p extrem1xtreme
docker push ${registry}/${project_name}/web:${version}